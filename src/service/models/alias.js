'use strict';

module.exports = {
  COMMENTS: `comments`,
  PUBLICATIONS: `publications`,
  ROLES: `roles`,
  CATEGORIES: `categories`,
  PUBLICATION_CATEGORIES: `publicationCategories`
};
